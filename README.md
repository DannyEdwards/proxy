# Proxy

Simple Firebase function based proxy server for making CORS requests from browser to any HTTP server.

Project has been initialised with `firebase-tools` and needs to be installed globally.

See `scripts` inside `functions/package.json` for development and deployment instructions.
