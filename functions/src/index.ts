import * as functions from "firebase-functions";
import * as https from "https";

export const proxy = functions
  .region("europe-west2")
  .https.onRequest((request, response) => {
    const { query } = request;
    if (query === null || !query.url || typeof query.url !== "string") {
      functions.logger.error("Bad request", { query });
      response.status(400);
      response.json({ error: "You must specify a `url` query parameter." });
      return;
    }
    try {
      https.get(query.url, (message) => {
        let body = "";
        message.setEncoding("utf8");
        message.on("data", (chunk) => (body += chunk));
        message.on("end", () => {
          // If we know it's JSON, parse it
          if (message.headers["content-type"] === "application/json") {
            body = JSON.parse(body);
          }
          functions.logger.info("Body recieved", { body });
          response.status(message.statusCode || 500);
          response.header("content-type", message.headers["content-type"]);
          response.header("access-control-allow-origin", "*");
          response.send(body);
        });
      });
    } catch (error) {
      functions.logger.error("Request error", { error });
      if ((error as HTTPSError)?.code === "ERR_INVALID_URL") {
        response.status(400);
        response.json({ error: "You must provide a valid `url`." });
        return;
      }
      response.status(500);
      response.json({ error: "There has been a server error." });
    }
  });
