declare type HTTPSError = {
  input: string | number;
  code: string;
};
